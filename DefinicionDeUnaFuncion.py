#Función que, tomando una lista de elementos ai compute y devuelva el siguiente valor:
#a1+2*Σ^n(ai)
"""El tipo de entrada son tuplas y el de salida es tipo int"""
def sum_product(my_list: list) -> int:
    x = my_list[0]+2*sum(my_list[1:])
    return x

"""Este es el docstring de la función"""
print (sum_product([2,3,5]))