# Instrucciones

# Simple statement: one logical line
# - Expression statements: mostly interactively to compute or write a value or a procedure
#   (function without results)
"""
a + 3
3 + 2
"""
# - Assignment statement: bind or rebind names to values or to modify items for mutable
#   objects. Annotation (type hinting is possible):
a = 3
b: int = 3

# - Augmented assignment statement
i=2
j=3
i += 6
j *= 8

# - The assert statement (for debugging)
assert a == 1

#- return: in a function. return leaves the current function call with the expression as its
#return value
x=6
def f(x):
    return x + 1

#-del: removes the binding to that name
del a

#- yield (advanced, in generator function)
def infinite_sequence():
    num = 0
    while True:
        yield num
        num += 1
gen = infinite_sequence()
next(gen)

#-raise (advanced, raising Exceptions)
def func():
    raise IOError
try:
    func()
except IOError as exc:
    raise RuntimeError('Failed to open database') from exc

#- break: for or while loop, terminates the nearest enclosing loop, skipping the else clause if
#the loop has one
for val in "string":
    if val == "i":
        break
    print(val)

#- continue: for or while loop, It continues with the next cycle of the nearest enclosing
#loop.

for val in "string":
    if val == "i":
        continue
    print(val)

#- import: find a module, loading and initializing it if necessary. We will see this when we
#see modules and packages

#- Others (advanced): global, nonlocal, pass

# Compound statement: contain (groups of) other statements
#- They affect or control the execution of those other statements in some way

#- The if, while and for statements implement traditional control flow constructs.

#- try specifies exception handlers and/or cleanup code for a group of statements

#- with statement allows the execution of initialization and finalization code around a block
#of code.

#- Function and class definitions are also syntactically compound statements.









