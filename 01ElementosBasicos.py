# Illustrate multiple elif example with an else
savings = float(input("Pon cuanto dinero tienes guardado: "))
if savings == 0:
    print("Lo siento no tienes nada")
elif savings < 500:
    print('Es una cantidad pequeña')
elif savings < 1000:
    print('Muy bien')
elif savings < 10000:
    print('Excelente')
else:
    print('Sigue asi, lo estas haciendo muy bien')
