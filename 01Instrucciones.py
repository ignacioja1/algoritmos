# Instrucciones de Asignación
name = 'Pedro'
# Instrucciones de lectura
user_name = input('Pon tu nombre: ')

f = open("demofile2.txt", "r")  # No encuentro este fichero
print(f.read())

# Instrucciones de escritura
f = open("demofile2.txt", "w")
f.write("Woops! I have deleted the content!")
f.close()

# Instrucciones de bifurcación
    # Bifurcación adelante
    # Bifurcación atrás
    # Bifurcación incondicional
    # Bifurcación condicional
# Obtain a number from the user
# and check to see if it is non-negative
num = int(input('Pon tu nombre: '))
if num < 0:
    print(num, 'es negativo')
print('-' * 25)
