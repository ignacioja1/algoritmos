def sum_list(start:int,end:int = 10) -> int:
    """
    Returns the sum of integers between start and end.
    :param start: first integer
    :param end: last integer
    :return: the sum
    """
    new_list = list(range(start,end+1))
    s = sum(new_list)
    return s

a = sum_list(1,4)
print(a)

