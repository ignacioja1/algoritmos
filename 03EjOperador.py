# Precedence of or & and
meal = "fruit"
money = 0
if (meal == "fruit" or meal == "sandwich") and money >= 2:
    print("Comida siendo entregada")
else:
    print("No se puede llevar la comida")


#Con () sale que no se puede repartir pero sin paréntesis si que se puede repartir
