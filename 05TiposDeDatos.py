#Tipos de Datos

#NONE

#Números
1 # int
True # boolean
1e5 # float
complex(1,3) # complex

#Secuencias
#Ordered sets indexed by non-negative numbers

#§ built-in function len()

#§ Item i of sequence a is selected by a[i].

#§ support slicing: a[i:j] selects all items with index k
#such that i <= k < j.

#§ “step” parameter: a[i:j:k] all items of a with index
#x where
#    x = i + n*k, n >= 0 and i <= x < j.

#Secuencias Inmutables
#§ Strings: items of a string object are Unicode code units.
#            chr() and ord() (chr(97), ‘a’, ord(‘a’) == 97)
"This is a string"
result = 1
f"This is the result: {result}"

#§ Tuples: items are formed by comma-separated lists of expressions.
a = (2, 3)
b = ('A', 3, 5, 6)
b[0]
b[:3]

#§ Bytes: immutable array. The items are 8-bit bytes. bytes() used to construct bytes objects.
bytes([15, 15])
bytes('a', encoding='utf8')

#Secuencias Mutables
#§ Lists: items of a list are arbitrary Python objects
#         comma-separated list of expressions in square brackets
my_list = [1, 2, 3]
my_list2 = [1, 'a', 3]
my_list3 = [(1, 2), (2, 3)]

#§ Byte Arrays: bytearray() constructor
bytearray([15, 15])
bytearray('a', encoding='utf8')

#Sets
#• Unordered, finite sets of unique, immutable objects
#• No index, they can be iterated over
#• Fast membership testing, duplicates
#• Intersection, union, difference, and symmetric difference
#• Sets and Frozen sets: (frozenset() immutable version, hashable)
my_set = {'a', 'b', 'c'}
'a' in my_set
my_set_2 = {'b', 'c'}
my_set.union(my_set_2)
my_set_2.add('e')
my_set_2.remove('b')
fs = frozenset({'a', 'b', 'c'})

#Mappings
#§ Dictionaries:
#    • finite sets of objects indexed by a value
#    • keys must be immutable (hashing required)
#    • subscript notation a[k] selects
cities = {'Wales': 'Cardiff',
          'England': 'London',
          'Scotland': 'Edinburgh',
          'Northern Ireland': 'Belfast',
          'Ireland': 'Dublin'}
print(len(cities))
cities['Wales']
cities.get('Ireland')
print(cities.values())
print(cities.keys())
print(cities.items())
print('Wales' in cities)
print('France' not in cities)

#Callables
#§ User-defined functions (see section Function definitions)
#   • Special attributes: __doc__, __name__, __module__, __dict__
def print_msg():
    print('Hello World!')
print_msg()
print(type(print_msg))
def print_my_msg(msg):
    print(msg)
print_my_msg('Hello World')
def square(n):
    return n * n
# Store result from square in a variable
result = square(4)
print(result)
#§ Built-in functions:
#   • wrapper around a C function, e.g. math.sin()
import math
math.sqrt(4)
#§ Classes:
#   • factories for new instances
#   • __init__() to initialize the new instance
#§Class instances can be made callable by defining a __call__() method in their class
#§ Generator functions
#• function or method which uses the yield statement is called a generator function.
#• Such a function, when called, returns an iterator object which can be used to execute the body of the function
#• calling the iterator’s __next__() method will cause the function to execute until it provides a value using the yield statement.

#Modules:
#§ imported by the import statement
#§ module object has a namespace implemented by a dictionary object (this is the dictionary referenced by the __globals__ attribute of functions defined in the module).
#§ Attribute references are translated to lookups in this dictionary, e.g., m.x is equivalent to m.__dict__["x"].

#Files:
#§ represents an open file. File objects are created by the open() built-in function
#§ sys.stdin, sys.stdout and sys.stderr are initialized to file objects corresponding to the interpreter’s standard input, output and error streams

#Custom classes:
#§ A class object can be called (see above) to yield a class instance (see below).
#§ A class has a namespace implemented by a dictionary object.
#§ When the attribute name is not found there, the attribute search continues in the base classesç

#Class instances:
#§ A class instance is created by calling a class object (see above).
#§ Attribute assignments and deletions update the instance’s dictionary

