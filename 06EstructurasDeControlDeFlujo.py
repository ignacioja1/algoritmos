#Estructuras de Control de Flujo

#Conditional Flow Control
#If statement in python
#Flow control based on a condition:
#Choice point that will be evaluated to True or False, typically combining:
#• Comparison operators: operator that return Boolean values. Ej: ==, >=, !=
#• Logical operators: used to combine two or more comparison expressions together. Ej: and, or, not
#• Identity operators: Ej: is, is not
#• Membership operators: Ej: in, not in

#if <condition-evaluating-to-boolean>:
#       statement
#Ejemplo 1:
meal = "fruit"
money = 0
if (meal == "fruit" or meal == "sandwich") and money >= 2:
    print("Merienda siendo entregada")

#Ejemplo 2 (Else):
meal = "fruit"
money = 90
if (meal == "fruit" or meal == "sandwich") and money >=2:
    print("Merienda siendo entregada")
else:
    print("No se puede entregar tu merienda")

#Ejemplo 3 (Elif):
savings = float(input("Introduce cuanto tienes ahorradop: "))
if savings < 0:
    print('Mala suerte')
elif savings == 0:
    print('Lo siento no tienes nada')
elif savings < 500:
    print('Es una cantidad pequeña')
elif savings < 1000:
    print('Muy bien')
elif savings < 10000:
    print('Excelente!')
else:
    print('Muchas gracias')

#Ejemplo 4 (Nested if):
snowing = True
temp = -1
if temp < 0:
    print('It is freezing')
    if snowing:
        print('Put on boots')
    print('Time for Hot Chocolate')
print('Bye')

#Ejemplo 5 (If):
age = 15
status = None
if age > 12 and age < 20:
    status = 'teenager'
else:
    status = 'not teenager'
print(status)
#Versión acortada
status = ('teenager' if age > 12 and age < 20 else 'not teenager')
print(status)

#Ejemplo 6 (If):
num = int(input('Enter a simple number: '))
result = (-1 if num < 0 else 1)
print('Result is ', result)



#Iterative Flow Control
#Two types: while loop and for loop

#while <test-condition-is-true>:
#       statement or statements
count = 0
print('Starting')
while count < 10:
    print(count)
    count += 1
print('Done')

#for i = from 0 to 10
#   statement or statements
print('Print out values in a range')
for i in range(0, 10):
    print(i, ' ', end='')
print()
print('Done')

#Ejemplo 1 (Over sequences):
words = ['cat', 'window', 'defenestrate']
for w in words:
    print(w, len(w))

#Ejemplo 2 (Combining):
a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print(i, a[i])

#Ejemplo 3 (Break):
print('Only print code if all iterations completed')
num = int(input('Enter a number to check for: '))
for i in range(0, 6):
    if i == num:
        break
    print(i, ' ', end='')

#Ejemplo 4 (Continue):
for i in range(0, 10):
    print(i, ' ', end='')
    if i % 2 == 1:
        continue
    print('hey its an even number ')
    print('we love even numbers ' )
print('Done')

























































































