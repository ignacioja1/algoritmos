Expresiones

# Expresión: computación que genera un valor
"""
9 + 5
Atoms: names, literals (my_value, 3.0)
Attribute references: (client.name)
Subscriptions: s[0]
Slicing: s[0:3]
Calls: my_function()
Operations:
Unary operators: -3
Binary operations: 3 < 7

TIPOS
• Arithmetic operators
• Comparison operators
• Logical operators
• Identity operators
• Membership operators
• Bitwise operators
"""
